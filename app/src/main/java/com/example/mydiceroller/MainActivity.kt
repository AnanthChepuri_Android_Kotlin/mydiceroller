package com.example.mydiceroller

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.time.chrono.HijrahChronology

class MainActivity() : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var resultText : TextView = findViewById(R.id.result_text)

        val rollButton: Button = findViewById(R.id.roll_button)
        rollButton.setOnClickListener { rollDice() }

        val rollUpButton: Button = findViewById(R.id.rollUp_button)
        rollUpButton.setOnClickListener { rollUpDice() }

        val resetBtn: Button = findViewById(R.id.reset_Button)
        resetBtn.setOnClickListener { restBtnClick() }
    }

    private fun rollDice() {

        val randomInt= (1..6).random()
        Toast.makeText(this,"Button clicked",
        Toast.LENGTH_SHORT).show()
        this.result_text.text = randomInt.toString()
    }

    private fun rollUpDice() {
        if (this.result_text.text.toString() == "Hello World!") {
            this.result_text.text = "1"
        }
        else if (this.result_text.text.toString().toInt() < 6) {
            var i = this.result_text.text.toString().toInt() + 1
            this.result_text.text = i.toString()
        } else {
            this.result_text.text = "6"
        }
    }

    fun restBtnClick() {
        print("Reset Clicked")
    }
}
